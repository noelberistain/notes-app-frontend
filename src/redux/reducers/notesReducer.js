import {
  ADD_NOTE,
  // LOAD_NOTES,
  UPDATE_NOTE,
  DELETE_NOTE,
  // UPDATE_NOTE_TITLE,
} from '../constants'
import { getDate } from '../../utils/getDate'

const initialState = [
  {
    id: 1,
    title: 'one',
    content: 'content for ONE',
    date: 'January 01,2020',
    // isUpdating: false,
  },
  {
    id: 2,
    title: 'two',
    content: 'content for TWO',
    date: 'February 02, 2020',
    // isUpdating: false,
  },
  {
    id: 3,
    title: 'three',
    content: 'content for THREE',
    date: 'February 02, 2020',
    // isUpdating: false,
  },
  {
    id: 4,
    title: 'four',
    content: '',
    date: 'Janaury 02, 2020',
    // isUpdating: false,
  },
  {
    id: 5,
    title: 'five',
    content: 'content for note number five',
    date: 'Janaury 02, 2020',
    // isUpdating: false,
  },
]

export const notesAppReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_NOTE:
      const newNote = {
        title: action.payload.title,
        content: action.payload.content,
        date: getDate(),
      }
      return [...state, newNote]
    case DELETE_NOTE:
      return state.filter((note) => action.payload !== note.title)
    case UPDATE_NOTE:
      return state.map((note) => {
        if (note.id === action.payload.id) {
          if (action.payload.title) {
            note.title = action.payload.title
          }
          if (action.payload.content) {
            note.content = action.payload.content
          }
        }
        return note
      })
    default:
      return state
  }
}
