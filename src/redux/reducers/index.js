import { notesAppReducer } from './notesReducer'
import { combineReducers } from 'redux'

export const rootReducer = combineReducers({
  notesList: notesAppReducer,
})
