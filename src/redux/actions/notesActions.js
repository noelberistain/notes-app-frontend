import {
  ADD_NOTE,
  DELETE_NOTE,
  UPDATE_NOTE,
  // LOAD_NOTES
} from '../constants'

const addNote = (payload) => {
  return { type: ADD_NOTE, payload }
}

const deleteNote = (payload) => {
  return { type: DELETE_NOTE, payload }
}

const updateNote = (payload) => {
  return { type: UPDATE_NOTE, payload }
}

export { addNote, deleteNote, updateNote }
