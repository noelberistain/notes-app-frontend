import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { Grid } from '@material-ui/core'
import { MainPage } from './components/main/MainPage'
import { LoginButton } from './components/buttons/LoginButton'

function App() {
  return (
    <Grid container justify="center" alignItems="center">
      <Switch>
        <Route path="/" component={LoginButton} exact />
        <Route path="/home-page" component={MainPage} />
      </Switch>
    </Grid>
  )
}

export default App
