import React from 'react'
import { NotesList } from '../notes/NotesList'
import { Header } from '../header/Header'
import { Grid } from '@material-ui/core'

export const MainPage = () => {
  return (
    <Grid container>
      <Header />
      <NotesList />
    </Grid>
  )
}
