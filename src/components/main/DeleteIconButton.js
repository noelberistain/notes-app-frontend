import React from 'react'
import { useDispatch } from 'react-redux'
import { IconButton } from '../common/index'
import { Delete as MUIconDelete } from '@material-ui/icons'
import { deleteNote } from '../../redux/actions'

export const DeleteIcon = (props) => {
  const { title, ...rest } = props
  const dispatch = useDispatch()
  const handleDelete = (event) => {
    event.preventDefault()
    dispatch(deleteNote(title))
  }
  return (
    <IconButton
      color="secondary"
      aria="delete"
      onClick={(event) => handleDelete(event)}
      {...rest}
    >
      <MUIconDelete />
    </IconButton>
  )
}
