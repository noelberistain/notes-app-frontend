import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {
  Avatar,
  Button,
  Card,
  CardContent,
  CardHeader,
  TextField,
} from '../common/index'
import { red } from '@material-ui/core/colors'
import { DeleteIcon } from '../main/DeleteIconButton'
import { getDate } from '../../utils/getDate'
import { useDispatch } from 'react-redux'
import { updateNote } from '../../redux/actions'
import { firstLetter } from '../../utils/firstLetter'

const useStyles = makeStyles(() => ({
  root: {
    maxWidth: 345,

    '& .MuiTextField-root': {
      width: '100%',
    },
  },
  avatar: {
    backgroundColor: red[500],
  },
}))

export const Note = ({ id, title, content }) => {
  const [updateContent, setUpdateContent] = useState(false)
  const [updateTitle, setUpdateTitle] = useState(false)
  const [noteLocalContent, setNoteLocalContent] = useState(content)
  const [noteLocalTitle, setNoteLocalTitle] = useState(title)
  const dispatch = useDispatch()
  const classes = useStyles()

  const onUpdateContent = () => {
    setUpdateContent(() => !updateContent)
  }

  const handleContentChange = (event) => {
    setNoteLocalContent(event.target.value)
  }

  const updatingContent = (event, id, content) => {
    event.preventDefault()
    onUpdateContent()
    dispatch(updateNote({ id, content }))
  }

  const onUpdateTitle = () => {
    setUpdateTitle(() => !updateTitle)
  }

  const handleTitleChange = (event) => {
    setNoteLocalTitle(event.target.value)
  }
  const updatingTitle = (event, id, title) => {
    event.preventDefault()
    onUpdateTitle()
    dispatch(updateNote({ id, title }))
  }

  return (
    <Card className={classes.root}>
      <CardHeader
        avatar={
          <Avatar
            aria="note"
            className={classes.avatar}
            content={firstLetter(noteLocalTitle)}
          />
        }
        action={<DeleteIcon title={noteLocalTitle} />}
        title={
          <TextField
            helperText={
              updateTitle && (
                <Button
                  color="primary"
                  label="Save"
                  size="small"
                  variant="text"
                  handleClick={(event) => {
                    updatingTitle(event, id, noteLocalTitle)
                  }}
                />
              )
            }
            value={noteLocalTitle}
            handleChange={(event) => handleTitleChange(event)}
            handleClick={onUpdateTitle}
          />
        }
        date={getDate()}
      />
      <CardContent>
        <TextField
          id="outlined-multiline-static"
          multiline
          variant="outlined"
          helperText={
            updateContent && (
              <Button
                color="primary"
                label="Save"
                size="small"
                variant="text"
                handleClick={(event) =>
                  updatingContent(event, id, noteLocalContent)
                }
              />
            )
          }
          value={noteLocalContent}
          handleChange={(event) => handleContentChange(event)}
          handleClick={onUpdateContent}
        />
      </CardContent>
    </Card>
  )
}
