import React from 'react'
import { useSelector } from 'react-redux'
import { getNotes } from '../../redux/selectors'
import { Note } from './Note'
import { Grid } from '@material-ui/core'

export const NotesList = () => {
  const notes = useSelector((state) => getNotes(state))
  const list =
    notes.length > 0
      ? notes.map((note) => (
          <Grid item xs={12} sm={6} md={3} key={note.id}>
            <Note
              title={note.title}
              content={note.content}
              date={note.date}
              id={note.id}
            />
          </Grid>
        ))
      : 'Add a new note'
  return (
    <Grid container spacing={2}>
      {list}
    </Grid>
  )
}
