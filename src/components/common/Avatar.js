import React from 'react'
import { Avatar as MUIAvatar } from '@material-ui/core'

export const Avatar = (props) => {
  const { aria, content, ...rest } = props
  return (
    <MUIAvatar aria-label={aria} {...rest}>
      {content}
    </MUIAvatar>
  )
}
