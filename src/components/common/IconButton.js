import React from 'react'
import { IconButton as MUIIconButton } from '@material-ui/core'

export const IconButton = (props) => {
  const { color, aria, ...rest } = props
  return <MUIIconButton aria-label={aria} color={color} {...rest} />
}
