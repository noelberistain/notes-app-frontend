import React from 'react'
import { Button as MUIButton } from '@material-ui/core'

export const Button = ({
  color = 'default',
  label,
  size,
  variant = 'contained',
  handleClick,
}) => {
  return (
    <MUIButton
      color={color}
      size={size}
      variant={variant}
      onClick={handleClick}
    >
      {label}
    </MUIButton>
  )
}
