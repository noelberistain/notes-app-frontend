import React from 'react'
import { TextField as MUITextField } from '@material-ui/core'

export const TextField = (props) => {
  const { title, handleChange, handleClick, ...rest } = props

  return (
    <MUITextField
      multiline
      varian="filled"
      onClick={handleClick}
      onChange={handleChange}
      {...rest}
    />
  )
}
