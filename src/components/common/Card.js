import React from 'react'
import { Card as MUICard } from '@material-ui/core'

export const Card = (props) => {
  return <MUICard {...props} />
}
