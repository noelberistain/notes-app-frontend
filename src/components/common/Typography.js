import React from 'react'
import { Typography as MUITypography } from '@material-ui/core'

export const Typography = (props) => {
  const {
    color = 'textSecondary',
    component = 'p',
    content,
    variant = 'body2',
    handleClick,
    ...rest
  } = props
  return (
    <>
      {content && (
        <MUITypography
          color={color}
          component={component}
          variant={variant}
          onClick={handleClick}
          {...rest}
        >
          {content}
        </MUITypography>
      )}
    </>
  )
}
