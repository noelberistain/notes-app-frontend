import React from 'react'
import { LogoutButton } from '../buttons/LogoutButton'
import { Grid, makeStyles } from '@material-ui/core'
import { AddNoteButton } from '../buttons/AddNoteButton'
const useStyles = makeStyles(() => ({
  headerItems: {
    textAlign: 'center',
    margin: 'auto',
  },
}))
export const Header = () => {
  const classes = useStyles()
  return (
    <Grid container>
      <Grid className={classes.headerItems} item xs={4}>
        <h1>Hi {'Name'}.</h1>
      </Grid>
      <Grid className={classes.headerItems} item xs={4}>
        <AddNoteButton />
      </Grid>
      <Grid className={classes.headerItems} item xs={4}>
        <LogoutButton />
      </Grid>
    </Grid>
  )
}
