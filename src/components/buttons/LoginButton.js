import React from 'react'
import { Button } from '../common/index'

export const LoginButton = () => {
  const handleLogin = () => {
    console.log('login has been Clicked')
  }
  return (
    <Button
      color="primary"
      label="Login with Google"
      variant="contained"
      handleClick={handleLogin}
    />
  )
}
