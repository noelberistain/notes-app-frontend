import React from 'react'

import { Button } from '../common/index'
import { useDispatch } from 'react-redux'
import { addNote } from '../../redux/actions'

export const AddNoteButton = () => {
  const dispatch = useDispatch()

  const handleAddNote = () => {
    const emptyNote = { title: '', content: '' }
    dispatch(addNote(emptyNote))
  }
  const handleClick = () => {
    handleAddNote()
  }

  return (
    <>
      <Button
        color="primary"
        label="Add a new Note"
        variant="contained"
        handleClick={handleClick}
      />
    </>
  )
}
