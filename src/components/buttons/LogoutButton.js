import React from 'react'
import { Button } from '../common/index'

export const LogoutButton = () => {
  const handleLogout = () => {
    console.log('logOut has been Clicked')
  }
  return (
    <Button
      label="Logout"
      variant="contained"
      handleClick={handleLogout}
      size="medium"
    />
  )
}
