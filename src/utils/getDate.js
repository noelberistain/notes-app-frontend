const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'November',
  'December',
]
export const getDate = () => {
  const date = new Date()
  return date.getMonth() > 8
    ? months[date.getMonth()]
    : months[date.getMonth()] +
        ' ' +
        (date.getDate() > 9 ? date.getDate() : '0' + date.getDate()) +
        ', ' +
        date.getFullYear()
}
